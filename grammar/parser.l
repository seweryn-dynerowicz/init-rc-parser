/***
   Copyright 2019 Seweryn Dynerowicz

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 ***/

/*** Definitions ***/
%{
  #include "node.h"
  #include "parser.tab.h"
%}

id-type1-syms [a-zA-Z0-9]
id-type2-syms [a-zA-Z0-9\-_]
id-type3-syms [a-zA-Z0-9\-_\.]
id-type4-syms [a-zA-Z0-9\-_\.+*#@|:,]

/* A {identifier-type1} is a non-empty sequence of {alnum} or dash (n.b. not as first or last) */
identifier-type1 {id-type1-syms}(-*{id-type1-syms}+)*
/* A {property-component} is a non-empty sequence of {alnum}, dash or underscore */
identifier-type2 {id-type2-syms}+

/* A {selinux-label} is composed of four {identifier-type2} separated by colons */
selinux-label {identifier-type2}:{identifier-type2}:{identifier-type2}:{identifier-type2}

/* An {identifier} is a non-empty sequence of {alnum}, dash, underscore, dot, plus, times, hash, at */
identifier-type3  {id-type3-syms}+

/* A {identifier-type4} is non-empty sequence of {alnum}, colon, dot, plus, hash, at, underscore or dash */
identifier-type4  {id-type4-syms}+

/* A {path} is sequence of {identifier-type4}, separated by slashes which may start or end with a slash */
path       \/?({identifier-type4}(\/{identifier-type4})*)\/?

/* The following lattice describes the relations of inclusion among all these regexps
   and thus provides us with their priority ordering in the lexer.

             {path}      -> Unix-style absolute or relative paths

               |_|

           {identifier-type4}

           /_/     \_\

    {identifier}   {selinux-label}

         |_|

   {property-name} -> AOSP properties (e.g. ro.hardware, sys.usb.debug)

    /_/       \_\

{domain}     {identifier-type2}

    \_\       /_/

   {identifier-type1}

 */

%x IN_LINE IN_ARGS IN_TRIG IN_MOUNT_FLAGS
%option noyywrap nodefault nounput yylineno

%% /* Rules */

<INITIAL>{
  import                { BEGIN(IN_LINE); return IMPORT; }
  on                    { BEGIN(IN_TRIG); return ON; }
  service               { BEGIN(IN_LINE); return SERVICE; }

  bootchart_init        { BEGIN(IN_LINE); return BOOTCHART; }
  check_fs              { BEGIN(IN_LINE); return CHECK_FS; }
  check_icd             { BEGIN(IN_LINE); return CHECK_ICD; }
  chmod                 { BEGIN(IN_LINE); return CHMOD; }
  chown                 { BEGIN(IN_LINE); return CHOWN; }
  class_reset           { BEGIN(IN_LINE); return CLASS_RESET; }
  class_start           { BEGIN(IN_LINE); return CLASS_START; }
  class_stop            { BEGIN(IN_LINE); return CLASS_STOP; }
  copy                  { BEGIN(IN_LINE); return COPY; }
  domainname            { BEGIN(IN_LINE); return DOMAINNAME; }
  enable                { BEGIN(IN_LINE); return ENABLE; }
  exec                  { BEGIN(IN_LINE); return EXEC; }
  export                { BEGIN(IN_LINE); return EXPORT; }
  ffu                   { BEGIN(IN_LINE); return FFU; }
  hostname              { BEGIN(IN_LINE); return HOSTNAME; }
  ifup                  { BEGIN(IN_LINE); return IFUP; }
  insmod                { BEGIN(IN_LINE); return INSMOD; }
  installkey            { BEGIN(IN_LINE); return INSTALLKEY; }
  load_system_props     { BEGIN(IN_LINE); return LOAD_SYSTEM_PROPS; }
  load_persist_props    { BEGIN(IN_LINE); return LOAD_PERSIST_PROPS; }
  loglevel              { BEGIN(IN_LINE); return LOGLEVEL; }
  mkdir                 { BEGIN(IN_LINE); return MKDIR; }
  mount_all             { BEGIN(IN_LINE); return MOUNT_ALL; }
  mount                 { BEGIN(IN_LINE); return MOUNT; }
  powerctl              { BEGIN(IN_LINE); return POWERCTL; }
  restart               { BEGIN(IN_LINE); return RESTART; }
  restorecon            { BEGIN(IN_LINE); return RESTORECON; }
  restorecon_recursive  { BEGIN(IN_LINE); return RESTORECON_RECURSIVE; }
  rm                    { BEGIN(IN_LINE); return RM; }
  rmdir                 { BEGIN(IN_LINE); return RMDIR; }
  setprop               { BEGIN(IN_LINE); return SETPROP; }
  setrlimit             { BEGIN(IN_LINE); return SETRLIMIT; }
  setusercryptopolicies { BEGIN(IN_LINE); return SETUSERCRYPTOPOLICIES; }
  start                 { BEGIN(IN_LINE); return START; }
  stop                  { BEGIN(IN_LINE); return STOP; }
  swapon_all            { BEGIN(IN_LINE); return SWAPON_ALL; }
  symlink               { BEGIN(IN_LINE); return SYMLINK; }
  sysclktz              { BEGIN(IN_LINE); return SYSCLKTZ; }
  trigger               { BEGIN(IN_LINE); return TRIGGER; }
  verity_load_state     { BEGIN(IN_LINE); return VERITY_LOAD_STATE; }
  verity_update_state   { BEGIN(IN_LINE); return VERITY_UPDATE_STATE; }
  wait                  { BEGIN(IN_LINE); return WAIT; }
  write                 { BEGIN(IN_LINE); return WRITE; }

  critical              { BEGIN(IN_LINE); return CRITICAL; }
  disabled              { BEGIN(IN_LINE); return DISABLED; }
  class                 { BEGIN(IN_LINE); return CLASS; }
  setenv                { BEGIN(IN_LINE); return SETENV; }
  user                  { BEGIN(IN_LINE); return USER; }
  group                 { BEGIN(IN_LINE); return GROUP; }
  seclabel              { BEGIN(IN_LINE); return SECLABEL; }
  oneshot               { BEGIN(IN_LINE); return ONESHOT; }
  console               { BEGIN(IN_LINE); return CONSOLE; }
  ioprio                { BEGIN(IN_LINE); return IOPRIO; }
  keycodes              { BEGIN(IN_LINE); return KEYCODES; }
  socket                { BEGIN(IN_LINE); return SOCKET; }
  writepid              { BEGIN(IN_LINE); return WRITEPID; }
  onrestart             return ONRESTART;

  \n+
  ^[ \t]*\n
  ^[ \t]*#.*\n
}

<IN_ARGS>{
  .*              return ARGUMENTS;
  .*\\\n          return ARGUMENTS;
  \n              { BEGIN(INITIAL); return EOL; }
}

<IN_MOUNT_FLAGS>{
  /* Mount flags */
  noatime         return NOATIME;
  noexec          return NOEXEC;
  nosuid          return NOSUID;
  nodev           return NODEV;
  nodiratime      return NODIRATIME;
  ro              return RO;
  rw              return RW;
  remount         return REMOUNT;
  bind            return BIND;
  rec             return REC;
  unbindable      return UNBINDABLE;
  private         return PRIVATE;
  slave           return SLAVE;
  shared          return SHARED;
  defaults        return DEFAULTS;
  wait            return WAIT;

  .*              return FSOPTIONS;
}

<IN_TRIG>{
  N\/A            return NA;
  &&              return AND;
  \.              return DOT;
  ,               return COMMA;
  =               return EQUALS;
  \*              return ASTERISK;
  property:       return PROPERTY;

  {identifier-type3}    { yylval.string = strdup(yytext); return IDENTIFIER_TYPE3; }
}

<IN_LINE>{
  /* Socket types */
  dgram           return DGRAM;
  stream          return STREAM;
  seqpacket       return SEQPACKET;
  /* IO Scheduling classes */
  rt              return RT;
  be              return BE;
  idle            return IDLE;

  \".*\"          return ESCAPED_STRING;

  $\{             return EXPANSION_OP;
  \}              return EXPANSION_CL;

  \.              return DOT;
  ,               return COMMA;
  =               return EQUALS;
  -               return SINGLEDASH;
  --              return DOUBLEDASH;

  [0-9]+          return DIGITS;

  {identifier-type1}    { yylval.string = strdup(yytext); return IDENTIFIER_TYPE1; }
  {identifier-type2}    { yylval.string = strdup(yytext); return IDENTIFIER_TYPE2; }
  {identifier-type3}    { yylval.string = strdup(yytext); return IDENTIFIER_TYPE3; }

  {selinux-label}       return SELINUX_LABEL;

  {path}                { yylval.string = strdup(yytext); return PATH; }
  {path}$\{             { yylval.string = strdup(yytext); return PATH_WITH_EXPANSION_OP; }
  \}{path}              { yylval.string = strdup(yytext); return PATH_WITH_EXPANSION_CL; }

  <<EOF>>         { static int once = 0; return once++ ? 0 : EOL; }
}

<IN_TRIG,IN_LINE,IN_ARGS,IN_MOUNT_FLAGS>{
  \\\n            /* Line-folding */
  (#.*)?\n        { BEGIN(INITIAL); return EOL; }
}

<*>{
  [ \t]+          /* Ignore white spaces and tabulations */
  .               /* Ignore everything else */
}

%%

/* Footer */

void begin(int start) { BEGIN(start); }
