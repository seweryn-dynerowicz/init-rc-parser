/***
   Copyright 2019 Seweryn Dynerowicz

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 ***/

/* Definitions */
/* TODO: FREE THE STRINGS ! */

%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <unistd.h>

  #define YY_HEADER_EXPORT_START_CONDITIONS
  #include "lex.yy.h"

  extern char* yytext;
  extern int yyleng;
  extern FILE *yyin;
  extern int yylineno;

  extern void begin(int start);

  extern int yylex();
  extern int yyparse();
  void yyerror(const char* str);

  #include "node.h"

  unsigned int printActions = 0;
  unsigned int printImports = 0;
  unsigned int printServices = 0;
  unsigned int printTriggers = 0;
  unsigned int printExecutions = 0;
  unsigned int printExpansions = 0;

  unsigned int printWarnings = 0;
%}

%union {
  char* string;
  unsigned int integer;
  node* node;
}

%token EOL

%token IMPORT

%token ON
%token BOOTCHART
%token CHMOD
%token CHOWN
%token CLASS_RESET CLASS_START CLASS_STOP
%token COPY
%token DOMAINNAME HOSTNAME
%token ENABLE
%token EXEC
%token EXPORT
%token IFUP
%token INSMOD
%token INSTALLKEY
%token LOAD_SYSTEM_PROPS LOAD_PERSIST_PROPS
%token LOGLEVEL
%token MKDIR RM RMDIR SYMLINK
%token MOUNT_ALL MOUNT FSOPTIONS
%token <string> NOATIME NOEXEC NOSUID NODEV NODIRATIME RO RW REMOUNT BIND REC UNBINDABLE PRIVATE SLAVE SHARED DEFAULTS
%token POWERCTL
%token START STOP RESTART
%token RESTORECON RESTORECON_RECURSIVE
%token SETPROP
%token SETRLIMIT
%token SETUSERCRYPTOPOLICIES
%token SWAPON_ALL
%token SYSCLKTZ
%token TRIGGER
%token VERITY_LOAD_STATE VERITY_UPDATE_STATE
%token WAIT
%token WRITE

 /* Samsung specific commands */
%token CHECK_FS CHECK_ICD FFU

%token SERVICE
%token CLASS
%token CONSOLE
%token CRITICAL
%token DISABLED
%token USER GROUP
%token IOPRIO RT BE IDLE
%token KEYCODES
%token ONESHOT
%token ONRESTART
%token SECLABEL
%token SETENV
%token SOCKET DGRAM STREAM SEQPACKET
%token WRITEPID

%token PROPERTY

%token NA AND DOT GAP COLON COMMA SLASH EQUALS SINGLEDASH DOUBLEDASH ASTERISK

%token <string> DOMAIN PROPERTY_NAME EXPANSION_OP EXPANSION_CL PATH PATH_WITH_EXPANSION_OP PATH_WITH_EXPANSION_CL ESCAPED_STRING DIGITS ARGUMENTS SELINUX_LABEL
%token <string> IDENTIFIER_TYPE1 IDENTIFIER_TYPE2 IDENTIFIER_TYPE3 IDENTIFIER_TYPE4

%type <string> trigger event property-condition property-match conjunction servicename path
%type <string> property-name property-value property-expansion
%type <string> identifier-list integer-list
%type <string> identifier-type1 identifier-type2 identifier-type3 identifier-type4

%%
/* Rules */
start: section
     | section start

section: import | action | service

import:  IMPORT path EOL { if(printImports) printf("Import: %s\n", $2); }

action:  ON trigger EOL commands { if(printActions) printf("Action: on %-16s\n", $2); }
trigger: event
       | event AND conjunction              { $$ = strcat(strcat($1, " && "), $3); }
       | property-condition
       | property-condition AND conjunction { $$ = strcat(strcat($1, " && "), $3); }

event: identifier-type3
conjunction: property-condition
           | conjunction AND property-condition { $$ = strcat(strcat($1, " && "), $3); }
property-condition: PROPERTY property-name EQUALS property-match { $$ = strcat(strcat($2, "="), $4); }
property-match: property-value | ASTERISK { $$ = "**"; }

service: SERVICE servicename path arguments EOL options { if(printServices) printf("Service: %-16s (%s)\n", $2, $3); }
servicename: identifier-type3

commands:
        | commands option  EOL { if(printWarnings) printf("warning: l.%d an option appears under the scope of an action\n", yylineno - 1); }
        | commands command EOL

command: BOOTCHART
       | CHMOD mode path
       | CHOWN user path
       | CHOWN user group path
       | CLASS_RESET class
       | CLASS_START class
       | CLASS_STOP class
       | COPY source target
       | DOMAINNAME domain
       | ENABLE	servicename
       | EXEC context DOUBLEDASH path arguments          { if(printExecutions) printf("Execution:               (%s)\n", $4); }
       | EXPORT	name value
       | HOSTNAME hostname
       | IFUP interface
       | INSMOD module parameters
       | INSTALLKEY key
       | LOAD_SYSTEM_PROPS
       | LOAD_PERSIST_PROPS
       | LOGLEVEL level
       | MKDIR path opt-mode opt-usergroup
       | MOUNT_ALL fstab
       | MOUNT fstype device mountpoint flags fsoptions
       | POWERCTL pc-argument
       | RESTART servicename
       | RESTORECON arguments
       | RESTORECON_RECURSIVE path
       | RM arguments
       | RMDIR path
       | SETPROP property-name property-value
       | SETRLIMIT resource current maximum
       | SETUSERCRYPTOPOLICIES policy
       | START servicename
       | STOP servicename
       | SWAPON_ALL fstab
       | SYMLINK target source
       | SYSCLKTZ offset
       | TRIGGER event                                   { if(printTriggers) printf("Trigger : %s\n", $2); }
       | VERITY_LOAD_STATE
       | VERITY_UPDATE_STATE
       | WAIT file
       | WAIT file timeout
       | WRITE file content
       /* Samsung-specific */
       | CHECK_FS path fstype
       | CHECK_ICD
       | FFU

opt-mode:
        | DIGITS
opt-usergroup:
             | user group

options:
       | options command EOL { if(printWarnings) printf("warning: l.%d a command appears under the scope of a service\n", yylineno - 1); }
       | options option EOL

option: CLASS class
      | CONSOLE
      | CRITICAL
      | DISABLED
      | GROUP groups
      | IOPRIO scheduling priority
      | KEYCODES keycodes
      | ONESHOT
      | ONRESTART command
      | SECLABEL seclabel
      | SETENV name value
      | SOCKET sockname socktype mode
      | SOCKET sockname socktype mode user
      | SOCKET sockname socktype mode user group
      | SOCKET sockname socktype mode user group context
      | USER users
      | WRITEPID path

class: identifier-type3
context: seclabel
       | SINGLEDASH user
       | SINGLEDASH user groups
seclabel: SELINUX_LABEL
name: identifier-type3
value: path
     | path-list
     | DIGITS
hostname: identifier-type3
interface: identifier-type3

module: path
parameters:
          | parameters identifier-type3

pc-argument: property-expansion

key: path
level: DIGITS | property-expansion

fstab: path
fstype: identifier-type3

flag: NOATIME | NOEXEC | NOSUID | NODEV | NODIRATIME | RO | RW | REMOUNT | BIND | REC | UNBINDABLE | PRIVATE | SLAVE | SHARED | DEFAULTS | WAIT
flags: { begin(IN_MOUNT_FLAGS); } flags-rbld
flags-rbld:
          | flags-rbld flag

fsoptions:
         | fsoptions FSOPTIONS

arguments: { begin(IN_ARGS); } args-rbld
args-rbld:
         | args-rbld ARGUMENTS

device: path
mountpoint: path

property-value: path | identifier-list | integer-list | ESCAPED_STRING | NA { $$ = "N/A"; } | property-expansion
resource: DIGITS
current: DIGITS
maximum: DIGITS
policy: path
target: path
source: path
offset: DIGITS
file: path
timeout: DIGITS
content: path
       | integer-list
       | ESCAPED_STRING
       | property-expansion

scheduling: RT
          | BE
          | IDLE

priority: DIGITS

keycodes: integer-sequence
sockname: path
socktype: DGRAM
        | STREAM
        | SEQPACKET

identifier-list: identifier-type3
               | identifier-list COMMA identifier-type3

integer-sequence: DIGITS
                | integer-sequence DIGITS

integer-list: DIGITS
            | integer-list COMMA DIGITS

path-list: path COLON path
         | path-list COLON path

user: identifier-type3 | DIGITS
users: user
     | users user

group: identifier-type3 | DIGITS
groups: group
      | groups group

mode: DIGITS

property-expansion: EXPANSION_OP property-name EXPANSION_CL

domain: identifier-type1
      | domain DOT identifier-type1

property-name: identifier-type3

path: identifier-type4
    | PATH
| PATH_WITH_EXPANSION_OP property-name PATH_WITH_EXPANSION_CL { if(printExpansions) printf("Expansion: %s\n", $2); $$ = strcat($1, strcat($2, $3)); }

identifier-type4: IDENTIFIER_TYPE4 | identifier-type3
identifier-type3: IDENTIFIER_TYPE3 | identifier-type2
identifier-type2: IDENTIFIER_TYPE2 | identifier-type1
identifier-type1: IDENTIFIER_TYPE1

%%

int main(int argc, char* argv[]) {
#if YYDEBUG
  yydebug = 1;
#endif
  unsigned char* filename = NULL;
  unsigned int opt;
  while ((opt = getopt(argc, argv, "seatixwf:")) != -1) {
    switch(opt) {
    case 's':
      printServices = 1;
      break;
    case 'e':
      printExecutions = 1;
      break;
    case 'a':
      printActions = 1;
      break;
    case 't':
      printTriggers = 1;
      break;
    case 'i':
      printImports = 1;
      break;
    case 'x':
      printExpansions = 1;
      break;
    case 'w':
      printWarnings = 1;
      break;
    case 'f':
      filename = optarg;
      break;
    }
  }
  if(filename != NULL) {
    yyin = fopen(filename, "r");
    if(yyin == NULL) {
      perror(argv[1]);
      return EXIT_FAILURE;
    }
    yyparse();
    fclose(yyin);
  } else
    printf("No file provided\n");
}

void yyerror(const char *str) {
#if YYDEBUG
  if(yychar == EOL)
    printf("error: l.%d <EOL> {token=%s} received: %s\n", yylineno, yytname[yychar - 255], str);
  else
    printf("error: l.%d <%s> {token=%s} received: %s\n", yylineno, yytext, yytname[yychar - 255], str);
#else
  if(yychar == EOL)
    printf("error: l.%d <EOL> received: %s\n", yylineno, str);
  else
    printf("error: l.%d <%s> received: %s\n", yylineno, yytext, str);
#endif
  exit(EXIT_FAILURE);
}
