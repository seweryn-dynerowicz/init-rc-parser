#include <stdlib.h>

#include "node.h"

node* make_node(code_t code) {
  node* new_node = malloc(sizeof(node));
  new_node->code = code;
  return new_node;
}
