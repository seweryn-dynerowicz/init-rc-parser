#!/bin/bash

SUCCESS=success
FAILURE=failure
SIGSEGV=sigsegv
UNKNOWN=unknown

PARSER=build/parser

if [[ ! -f ${PARSER} ]]; then
    echo "Need to build ${PARSER}"
    make
fi

printf "Running tests ...\n"

FILES=$(find tests -name "init.*")
LONGEST=0
for f in ${FILES}; do
    LENGTH=${#f}
    if [[ LENGTH -gt LONGEST ]]; then
	LONGEST=$LENGTH;
    fi
done
LONGEST=$(($LONGEST+1))

for f in ${FILES}; do
  printf "%-${LONGEST}s: " "$f";
  $(${PARSER} $f > .test.output 2> /dev/null);
  RETURN=$(echo $?)
  if [ "${RETURN}" == 0 ] || [ "${RETURN}" == 1 ]; then
      ERROR_COUNT=$(grep -c ^error: .test.output)
      WARNING_COUNT=$(grep -c ^warning: .test.output)
      EXPANSION_COUNT=$(grep -c ^expansion .test.output)
      if [[ "$ERROR_COUNT" -ne "0" ]]; then
	  printf "\e[91;1m${FAILURE}\n";
      elif [[ "$WARNING_COUNT" -ne "0" ]]; then
	  printf "\e[93;1m${SUCCESS}\n";
      else
	  printf "\e[92;1m${SUCCESS}\e[0m\n";
      fi
      if [[ "$ERROR_COUNT" -ne "0" ]]; then
	  printf "\e[91;1m$(grep ^error: .test.output)\e[0m\n"
      fi
      if [[ "$WARNING_COUNT" -ne "0" ]]; then
	  printf "\e[93;1m$(grep ^warning: .test.output)\e[0m\n"
      fi
      if [[ "$EXPANSION_COUNT" -ne "0" ]]; then
	  printf "\e[93;1mwarning: property-expansions are ignored ($EXPANSION_COUNT)\e[0m\n"
      fi
  elif [ "$RETURN" == 139 ]; then
      printf "\e[1;101;93m${SIGSEGV}\e[0m\n"
  else
      printf "\e[94;1m${UNKNOWN}\e[0m\n"
  fi
done

rm .test.output
