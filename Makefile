GRAMMAR=grammar
SOURCE=src
INCLUDE=include
BUILD=build
REPORT=report

FLEX_FLAGS="--outfile=${SOURCE}/lex.yy.c"

all:
	@echo "Building parser ..."
	@bison --report=all --report-file=${REPORT}/report.txt --output=${SOURCE}/parser.tab.c --defines=${INCLUDE}/parser.tab.h ${GRAMMAR}/parser.y
	@flex --header-file=${INCLUDE}/lex.yy.h ${FLEX_FLAGS} ${GRAMMAR}/parser.l
	@gcc -I${INCLUDE}/ ${SOURCE}/*.c -o ${BUILD}/parser

debug:
	@echo "Building parser with debugging ..."
	@bison --report=all --report-file=${REPORT}/report.txt --verbose --debug --output=${SOURCE}/parser.tab.c --defines=${INCLUDE}/parser.tab.h ${GRAMMAR}/parser.y
	@flex --header-file=${INCLUDE}/lex.yy.h ${FLEX_FLAGS} ${GRAMMAR}/parser.l
	@gcc -I${INCLUDE}/ ${SOURCE}/*.c -o ${BUILD}/parser

debug-with-flex:
	@echo "Building parser with debugging on flex as well ..."
	@bison --report=all --report-file=${REPORT}/report.txt --verbose --debug --output=${SOURCE}/parser.tab.c --defines=${INCLUDE}/parser.tab.h ${GRAMMAR}/parser.y
	@flex --debug --header-file=${INCLUDE}/lex.yy.h ${FLEX_FLAGS} ${GRAMMAR}/parser.l
	@gcc -I${INCLUDE}/ ${SOURCE}/*.c -o ${BUILD}/parser

debug-only-flex:
	@echo "Building parser with debugging only on flex ..."
	@bison --report=all --report-file=${REPORT}/report.txt --output=${SOURCE}/parser.tab.c --defines=${INCLUDE}/parser.tab.h ${GRAMMAR}/parser.y
	@flex --debug --header-file=${INCLUDE}/lex.yy.h ${FLEX_FLAGS} ${GRAMMAR}/parser.l
	@gcc -I${INCLUDE}/ ${SOURCE}/*.c -o ${BUILD}/parser

clean:
	@echo "Cleaning ..."
	@rm -f ${SOURCE}/lex.yy.c ${SOURCE}/parser.tab.c ${INCLUDE}/parser.tab.h ${INCLUDE}/lex.yy.h ${BUILD}/* ${REPORT}/*
