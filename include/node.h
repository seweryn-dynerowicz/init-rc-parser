#ifndef __HAVE_NODE_H__
#define __HAVE_NODE_H__

#include <stdint.h>

typedef uint8_t code_t;

#define CMD_BOOTCHART             ((code_t) 0x01)
#define CMD_CHMOD                 ((code_t) 0x02)
#define CMD_CHOWN                 ((code_t) 0x03)
#define CMD_CLASS_RESET           ((code_t) 0x04)
#define CMD_CLASS_START           ((code_t) 0x05)
#define CMD_CLASS_STOP            ((code_t) 0x06)
#define CMD_COPY                  ((code_t) 0x07)
#define CMD_DOMAINNAME            ((code_t) 0x08)
#define CMD_ENABLE                ((code_t) 0x09)
#define CMD_EXEC                  ((code_t) 0x0a)
#define CMD_EXPORT                ((code_t) 0x0b)
#define CMD_HOSTNAME              ((code_t) 0x0c)
#define CMD_IFUP                  ((code_t) 0x0d)
#define CMD_INSMOD                ((code_t) 0x0e)
#define CMD_INSTALLKEY            ((code_t) 0x0f)
#define CMD_LOAD_SYSTEM_PROPS     ((code_t) 0x10)
#define CMD_LOAD_PERSIST_PROPS    ((code_t) 0x11)
#define CMD_LOGLEVEL              ((code_t) 0x12)
#define CMD_MKDIR                 ((code_t) 0x13)
#define CMD_MOUNT_ALL             ((code_t) 0x14)
#define CMD_MOUNT                 ((code_t) 0x15)
#define CMD_POWERCTL              ((code_t) 0x16)
#define CMD_RESTART               ((code_t) 0x17)
#define CMD_RESTORECON            ((code_t) 0x18)
#define CMD_RESTORECON_RECURSIVE  ((code_t) 0x19)
#define CMD_RM                    ((code_t) 0x1a)
#define CMD_RMDIR                 ((code_t) 0x1b)
#define CMD_SETPROP               ((code_t) 0x1c)
#define CMD_SETRLIMIT             ((code_t) 0x1d)
#define CMD_SETUSERCRYPTOPOLICIES ((code_t) 0x1e)
#define CMD_START                 ((code_t) 0x1f)
#define CMD_STOP                  ((code_t) 0x20)
#define CMD_SYMLINK               ((code_t) 0x21)
#define CMD_SWAPON_ALL            ((code_t) 0x22)
#define CMD_SYSCLKTZ              ((code_t) 0x23)
#define CMD_TRIGGER               ((code_t) 0x24)
#define CMD_VERITY_LOAD_STATE     ((code_t) 0x25)
#define CMD_VERITY_UPDATE_STATE   ((code_t) 0x26)
#define CMD_WAIT                  ((code_t) 0x27)
#define CMD_WRITE                 ((code_t) 0x28)

#define CMD_CHECK_FS              ((code_t) 0x80)
#define CMD_CHECK_ICD             ((code_t) 0x81)
#define CMD_FFU                   ((code_t) 0x82)

#define OPT_CLASS                 ((code_t) 0x40)
#define OPT_CONSOLE               ((code_t) 0x41)
#define OPT_CRITICAL		  ((code_t) 0x42)
#define OPT_DISABLED		  ((code_t) 0x43)
#define OPT_USER		  ((code_t) 0x44)
#define OPT_GROUP		  ((code_t) 0x45)
#define OPT_IOPRIO		  ((code_t) 0x46)
#define OPT_KEYCODES		  ((code_t) 0x47)
#define OPT_ONESHOT		  ((code_t) 0x48)
#define OPT_ONRESTART		  ((code_t) 0x49)
#define OPT_SECLABEL		  ((code_t) 0x50)
#define OPT_SETENV		  ((code_t) 0x51)
#define OPT_SOCKET		  ((code_t) 0x52)
#define OPT_WRITEPID              ((code_t) 0x53)

typedef struct {
  code_t code;
  uint32_t op_uint32;
  mode_t mode;
  unsigned char* op1;
  unsigned char* op2;
  unsigned char* op3;
  unsigned char* op4;
  unsigned char* op5;
  unsigned char* op6;
  void* nested;
} __attribute__((__packed__)) node;

node* make_node(code_t);

#endif
